import java.util.Comparator;

public class Player{
    private String name;
    private String savedgameFile;
    private int gamesplayed;
    private int gameswon;
    private int hintsUsed;
    private int totalGuesses;
    private int accuracy;
    private String savedEncrypted;
    private String savedUserPhrase;
    private String savedPhrase;
    private char savedType;

    public Player(String uname){
        this.name=uname;
        gamesplayed=0;
        gameswon=0;
        hintsUsed=0;
        totalGuesses=0;
        accuracy=0;
        savedPhrase="null";
        savedUserPhrase="null";
        savedEncrypted="null";
        savedType = 'm';
    }

    public Player(String uname, int gamesplayed, int gameswon, int hintsUsed, int totalGuesses, int accuracy, String savedEncrypted, String savedUserPhrase, String savedPhrase,char savedType){
        this.name = uname;
        this.gamesplayed = gamesplayed;
        this.gameswon = gameswon;
        this.hintsUsed = hintsUsed;
        this.totalGuesses = totalGuesses;
        this.accuracy = accuracy;
        this.savedEncrypted = savedEncrypted;
        this.savedUserPhrase = savedUserPhrase;
        this.savedPhrase = savedPhrase;
        this.savedType = savedType;
    }


    public static Comparator<Player> PlaGameWon = new Comparator<Player>() {
        @Override
        public int compare(Player o1, Player o2) {
            int p1 = o1.getGameswon();
            int p2 = o2.getGameswon();
            return p2-p1;
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSavedgameFile() {
        return savedgameFile;
    }

    public void setSavedgameFile(String savedgameFile) {
        this.savedgameFile = savedgameFile;
    }

    public int getGamesplayed() {
        return gamesplayed;
    }

    public void addGameplayed() {
        this.gamesplayed++;
    }

    public int getGameswon() {
        return gameswon;
    }

    public void addGameswon() {
        this.gameswon++;
        this.gamesplayed++;
    }
    public int getHintsUsed(){
        return hintsUsed;
    }

    public void addHintUsed(){
        hintsUsed++;
    }
    public int getTotalGuesses(){
        return totalGuesses;
    }
    public void addGuess(){
        totalGuesses++;
    }
    public int getAccuracy(){
        return accuracy;
    }

    public void addAccuracy(int numletters){
        accuracy+=numletters;
    }

	public String getSavedEncrypted() {
		return savedEncrypted;
	}

	public void setSavedEncrypted(String savedEncrypted) {
		this.savedEncrypted = savedEncrypted;
	}

	public String getSavedUserPhrase() {
		return savedUserPhrase;
	}

	public void setSavedUserPhrase(String savedUserPhrase) {
		this.savedUserPhrase = savedUserPhrase;
	}

	public String getSavedPhrase() {
		return savedPhrase;
	}

	public void setSavedPhrase(String savedPhrase) {
		this.savedPhrase = savedPhrase;
	}

	public char getSavedType() {
		return savedType;
	}

	public void setSavedType(char savedType) {
		this.savedType = savedType;
	}
}
