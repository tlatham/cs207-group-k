import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
public class Cryptogram {
    private String phrase;
    private String alphabet = "abcdefghijklmnopqrstuvwxyz";
    private String newPhrase;
    private ArrayList<Integer> newNumPhrase = new ArrayList<>();
    public Cryptogram(String userPhrase){
        phrase=userPhrase;
        encrypt();
        numencrypt();
    }

    public void encrypt(){
        char c;
        String updated=phrase;
        ArrayList<Integer> randNums = new ArrayList<>();
        ArrayList<Character> chars = new ArrayList<>();
        ArrayList<Character> charsAdded = new ArrayList<>();
        char[] phraseAsChar = phrase.toCharArray();
        for (int i=0;i<phrase.length();i++){
            charsAdded.add(phraseAsChar[i]);
        }

        newPhrase = phrase;
        int k;
        Random rnd = new Random();
        for (int i=0; i<phrase.length();i++){
            c=phrase.charAt(i);
            newPhrase = updated;
            if (c!=' '){
                if (!chars.contains(c)){
                    k = rnd.nextInt(26);
                    while(randNums.contains(k) || charsAdded.contains(alphabet.charAt(k))){
                        k = rnd.nextInt(26);
                    }
                    randNums.add(k);

                    if (!chars.contains(c)){
                        chars.add(c);

                    }
                    updated=newPhrase.replace(c, alphabet.charAt(k));
                }

            }

        }
        newPhrase = updated;

    }
    
    public void numencrypt(){
        char c;
        String updated=phrase;
        ArrayList<Integer> randNums = new ArrayList<>();
        ArrayList<Character> chars = new ArrayList<>();
        for(int i =0; i<updated.length();i++) {
        	newNumPhrase.add(0);
        }
        int k;
        Random rnd = new Random();
        for (int i=0; i<phrase.length();i++){
            c=phrase.charAt(i);
            if (c!=' '){
            	if (chars.contains(c)) {
            		k = randNums.get(chars.indexOf(c));
            	}else {
                k = rnd.nextInt(26) + 1;
                while(randNums.contains(k)){
                    k = rnd.nextInt(26) + 1;
                }
                randNums.add(k);
                if (!chars.contains(c)){
                    chars.add(c);
                }
            	}
                newNumPhrase.set(i,k);
                updated=updated.replaceAll(Character.toString(c), Integer.toString(k));
            }else {
            	newNumPhrase.set(i, 27);
            }

        }

    }
    public String getPhrase(){
        return phrase;
    }
    public void setPhrase(String change){
        phrase = change;
    }
    public String getEncrypted(){
        return  newPhrase;
    }
    public void setEncrypted(String change){
        newPhrase = change;
    }
    public ArrayList<Integer> getnumEncrypted(){
        return  newNumPhrase;
    }
    public void setNumEncrypted(ArrayList<Integer> change){
        newNumPhrase = change;
    }

    public int getnumofLetters(){
        int count=0;
        ArrayList<Character> c = new ArrayList<>();
        for (int i=0;i<phrase.length();i++){
            if (phrase.charAt(i)!=' ' && !c.contains(phrase.charAt(i))){
                count++;
                c.add(phrase.charAt(i));
            }
        }
        return count;
    }
}
