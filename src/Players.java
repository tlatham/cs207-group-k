import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Players  {
    private ArrayList<Player> players;


    public Players(String filename){

        players=new ArrayList<>();
        this.readFromFile(filename);
    }

    public void addPlayer(String name){
        players.add(new Player(name));
    }

    public Player findPlayer(String name){
        for (int i=0;i<players.size();i++){
            if (players.get(i).getName().equals(name)){
                return players.get(i);
            }
        }
        System.out.println("Player not Found! creating new Player");
        players.add(new Player(name));
        return players.get(players.size()-1);
    }

    public ArrayList<Player> getPlayers(){
        return players;
    }
    public void readFromFile(String filename){

        try
        {
            System.out.println("success..");
            BufferedReader input = new BufferedReader(new FileReader(filename));
            String jj = null;
           // if (input.readLine()!=null){
            while((jj = input.readLine())!=null)
                {
                    String [] words = jj.split(";");
                    String name = "";
                    String savedEncrypted = "";
                    String savedUserPhrase = "";
                    String savedPhrase = "";
                    char savedType = ' ';
                    int gamesplayed, gameswon, hintsUsed, totalGuesses, accuracy;
                    name=words[0];
                    gamesplayed = Integer.parseInt(words[1]);
                    gameswon = Integer.parseInt(words[2]);
                    hintsUsed = Integer.parseInt(words[3]);
                    totalGuesses = Integer.parseInt(words[4]);
                    accuracy = Integer.parseInt(words[5]);
                    savedEncrypted = words[6];
                    savedUserPhrase = words[7];
                    savedPhrase = words[8];
                    savedType = words[9].charAt(0);
                    Player p = new Player(name, gamesplayed, gameswon, hintsUsed, totalGuesses, accuracy, savedEncrypted, savedUserPhrase, savedPhrase, savedType);
                    players.add(p);

                }
            //} else{
              //  System.out.println("File was empty! no players found");
            //}

        }

    catch(IOException e)
        {
            e.printStackTrace();
        }

    }
    public void saveToFile(String filename){
        try{
            BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
            for (int i=0; i<players.size();i++){
                bw.write(players.get(i).getName()+ ";" + players.get(i).getGamesplayed() + ";" + players.get(i).getGameswon() + ";" + players.get(i).getHintsUsed() + ";" + players.get(i).getTotalGuesses() + ";" + players.get(i).getAccuracy() + ";" + players.get(i).getSavedEncrypted() + ";" + players.get(i).getSavedUserPhrase() + ";" + players.get(i).getSavedPhrase() + ";" + players.get(i).getSavedType() +"\n");


            }
            bw.close();
        }catch (IOException io){
            io.printStackTrace();
        }

    }
}
