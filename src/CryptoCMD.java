import java.util.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class CryptoCMD {
	ArrayList<String> cryptos = new ArrayList<String>();
	Players p = new Players("players.txt");
	ArrayList<Player> players;
	Scanner scan1 = new Scanner(System.in);
	boolean finished = false;
	boolean checked = false;
	private ArrayList<Integer> numEncrypted = new ArrayList<Integer>();
	private String encrypted;
	private String guess;
	private String[] brokenString = new String[3];
	private String userPhrase = "                                          ";
	int counter = 0;
	private Cryptogram c;
	Player curPlayer;
	private boolean loggedIn;
	Leaderboard leaderboard;

	public CryptoCMD() {
		this.selectPlayer();
		leaderboard = new Leaderboard(players);
		this.runCrypto();
	}

	public void selectPlayer() {
		players = p.getPlayers();
		Scanner s = new Scanner(System.in);
		while (loggedIn == false) {
			System.out.println(
					"> Enter a username, if that username doesn't exit, it will be created.If you are creating a username do not inclue semi colons.");
			if (s.hasNextLine()) {
				String line = s.nextLine();
				if (line.contains(";")) {
					System.out.println(">ERROR::::Do not inclue semi-colons in your name!.");
				} else {
					curPlayer = p.findPlayer(line);
					loggedIn = true;
				}
			}
		}
	}

	public void runCrypto() {

		String game = "";
		int i = 0;
		Random rnd = new Random();
		int k;
		try {
			File f = new File("cryptogram.txt");
			Scanner sc = new Scanner(f);
			while (sc.hasNextLine()) {
				cryptos.add(i, sc.nextLine());
			}
		} catch (FileNotFoundException e) {
			System.out.println("There is no file present.");
			System.exit(0);
		}
//        CryptogramViewer cv = new CryptogramViewer();
		while (!game.equals("exit")) {
			finished =false;
			userPhrase = "                                          ";
		k = rnd.nextInt(cryptos.size());
		c = new Cryptogram(cryptos.get(k));
		System.out.println(curPlayer.getSavedPhrase());
		System.out.println(curPlayer.getSavedEncrypted());
		System.out.println(curPlayer.getSavedUserPhrase());
		if (!(curPlayer.getSavedPhrase().equals("null") && curPlayer.getSavedEncrypted().equals("null") && curPlayer.getSavedUserPhrase().equals("null"))) {
			System.out.println(">You have a saved file would you like to load it (Y/N).");
			String answer = scan1.nextLine();
			if (answer.toUpperCase().charAt(0) == 'Y') {
				if (curPlayer.getSavedType() == 'L') {
					c.setPhrase(curPlayer.getSavedPhrase());
					c.setEncrypted(curPlayer.getSavedEncrypted());
					userPhrase = curPlayer.getSavedUserPhrase();
					encrypted = c.getEncrypted();
					letterCrypto(c);
				} else if (curPlayer.getSavedType() == 'N') {
					userPhrase = curPlayer.getSavedUserPhrase();
					c.setPhrase(curPlayer.getSavedPhrase());
					System.out.println(curPlayer.getSavedEncrypted());
					String[] nums = curPlayer.getSavedEncrypted().split(" ");
					for (int ii = 0; ii < nums.length; ii++) {
						System.out.println(nums[ii]);
						numEncrypted.add(Integer.parseInt(nums[ii]));
					}
					c.setNumEncrypted(numEncrypted);
					numCrypto(c);
				} else {
					System.out.println("error in user file");
					System.exit(0);
				}
			} else {
			}
		}
		//while (!game.equals("exit")) {
			System.out.println(
					"> Type L to play a letter cryptogram or N to play a number cryptogram, or 'exit' to exit!");
			System.out.println("> Type 'stats' to see your player statistics!");
			System.out.println("> Type 'leaderboard' to see the top players!");
			System.out.print("> ");
			game = scan1.nextLine();
			if (game.equals("L")) {
				System.out.println();
				encrypted = c.getEncrypted();
				letterCrypto(c);
			} else if (game.equals("stats")) {
				displayStats();
			} else if (game.equals("N")) {
				System.out.println();
				numEncrypted = c.getnumEncrypted();
				numCrypto(c);
			} else if (game.equals("leaderboard")){
				leaderboard.displayLeaderboard(players);
			}else{
				System.out.println();
			}
		}
		System.out.println("Exit selected, Writing player details to file.......");
		p.saveToFile("players.txt");
		System.exit(0);
	}

	public void numCrypto(Cryptogram c) {
		while (finished == false) {
			displayNumCrypto();
			guess = scan1.nextLine();
			checkNumGuess();
			checked = false;
			if (userPhrase.trim().equals(c.getPhrase().trim())) {
				curPlayer.addGameswon();
				curPlayer.addAccuracy(c.getnumofLetters());
				finished = true;
				System.out.println("> " + c.getPhrase());
				System.out.println("> Game Finished! You Won!");
				curPlayer.setSavedEncrypted("null");
				curPlayer.setSavedPhrase("null");
				curPlayer.setSavedType('m');
				curPlayer.setSavedUserPhrase("null");
			} else if (userPhrase.trim().replaceAll("\\s", "").length() == c.getPhrase().replaceAll("\\s", "").trim()
					.length()) {
				System.out.println(">>>Your answer is incorrect keep trying!<<<");
			}
		}
	}

	public void letterCrypto(Cryptogram c) {
		while (finished == false) {
			displayCrypto();
			guess = scan1.nextLine();
			checkGuess();
			checked = false;
			if (userPhrase.trim().equals(c.getPhrase().trim())) {
				curPlayer.addGameswon();
				curPlayer.addAccuracy(c.getnumofLetters());
				finished = true;
				System.out.println("> " + c.getPhrase());
				System.out.println("> Game Finished! You Won!");
				curPlayer.setSavedEncrypted("null");
				curPlayer.setSavedPhrase("null");
				curPlayer.setSavedType('m');
				curPlayer.setSavedUserPhrase("null");
			} else if (userPhrase.trim().replaceAll("\\s", "").length() == c.getPhrase().trim().replaceAll("\\s", "")
					.length()) {
				System.out.println(">>>Your answer is incorrect keep trying!<<<");
			}
		}
	}

	public void checkGuess() {
		String charString;
		String userString = userPhrase;
		char position, letterguess;
		if (guess.equals("hint")) {
			curPlayer.addHintUsed();
		} else if (guess.toLowerCase().equals("save")) {// code to save a letter crypto.
			if (curPlayer.getSavedType() == 'N' || curPlayer.getSavedType() == 'L') { // test if player already has
																						// saved game.
				System.out.print("> You already have a saved crypto do you wish to save? (Y/N)");
				String answer = scan1.nextLine();
				if (answer.toUpperCase().charAt(0) == 'Y') {
					System.out.println("Saving.");
					curPlayer.setSavedEncrypted(encrypted);
					curPlayer.setSavedPhrase(c.getPhrase());
					curPlayer.setSavedUserPhrase(userPhrase);
					curPlayer.setSavedType('L');
					System.out.println("save selected, Writing player details to file");
					p.saveToFile("players.txt");
				} else if (answer.toUpperCase().charAt(0) == 'N') {
					System.out.println("Not Saving.");
				} else {
					System.out.println("Incorrect argument.");
				}
			} else {
				curPlayer.setSavedEncrypted(encrypted);
				curPlayer.setSavedPhrase(c.getPhrase());
				curPlayer.setSavedUserPhrase(userPhrase);
				curPlayer.setSavedType('L');
				System.out.println("save selected, Writing player details to file");
				p.saveToFile("players.txt");
			}
		} else if (guess.toLowerCase().equals("exit")) {
			System.out.println("Exit selected, Writing player details to file");
			p.saveToFile("players.txt");
			System.exit(0);
		} else if (guess.toLowerCase().equals("undo")) {
			System.out.println("Enter leter you wish to undo.");
			System.out.print(">");
			char remove = scan1.nextLine().charAt(0);
			if (userPhrase.indexOf(remove) != -1) {
				userPhrase = userPhrase.replace(remove, ' ');
				System.out.println("The character '" + remove + "' has been removed.");
			} else {
				System.out.println("The character you entered has not been mapped.");
			}
		} else if (guess.toLowerCase().equals("reveal")) {
			System.out.println("> " + c.getPhrase());
			System.out.println("> Game Finished, you lost!");
			finished = true;
			curPlayer.addGameplayed();
		} else {
			curPlayer.addGuess();
			brokenString = guess.split(" ");
			letterguess = brokenString[0].charAt(0);
			position = brokenString[1].charAt(0);
			if (encrypted.indexOf(position) == -1) {
				System.out.println("That position does not exist in the encrypted string!");
			} else if (userString.contains(Character.toString(letterguess))) {
				System.out.println("you have already entered the letter " + letterguess);
			} else {
				charString = encrypted;
				if (userString.charAt(charString.indexOf(position)) != ' ') {
					System.out.println("> Are you sure you want to overwrite this answer?(y/n)");
					char remove = scan1.nextLine().charAt(0);
					if (remove == 'y') {
						userString = userString.replace(userString.charAt(charString.indexOf(position)), letterguess);
						System.out.println("> Your character was overwritten.");
						checked = true;
					} else if (remove == 'n') {
						System.out.println("> Your character was not overwritten.");
					}
				} else {
					for (int i = 0; i < charString.length(); i++) {
						if (charString.charAt(i) == position) {
							// userString = userString.substring(0, i)+userString.substring(i+1);
							userString = userString.substring(0, i) + letterguess + userString.substring(i + 1);

						}
					}
				}
				userPhrase = userString;
			}

		}
	}

	public void checkNumGuess() {
		ArrayList<Integer> intString = new ArrayList<Integer>();
		String userString = userPhrase;
		char letterguess;
		int position;
		if (guess.equals("hint")) {

		} else if (guess.toLowerCase().equals("save")) {// code to save a number crypto
			if (curPlayer.getSavedType() == 'N' || curPlayer.getSavedType() == 'L') { // test if player already has
																						// saved game.
				System.out.print("> You already have a saved crypto do you wish to save? (Y/N)");
				String answer = scan1.nextLine();
				if (answer.toUpperCase().charAt(0) == 'Y') {
					System.out.println("Saving.");
					String move = "";
					for (int ii = 0; ii < numEncrypted.size(); ii++) {
						if (ii != 0) {
							move = move + " ";
						}
						move = move + Integer.toString(numEncrypted.get(ii));
					}
					curPlayer.setSavedEncrypted(move);
					curPlayer.setSavedPhrase(c.getPhrase());
					curPlayer.setSavedUserPhrase(userPhrase);
					curPlayer.setSavedType('L');
					System.out.println("save selected, Writing player details to file");
					p.saveToFile("players.txt");
				} else if (answer.toUpperCase().charAt(0) == 'N') {
					System.out.println("Not Saving.");
				} else {
					System.out.println("Incorrect argument.");
				}
			} else {
				System.out.println("Save selected");
				String move = "";
				for (int ii = 0; ii < numEncrypted.size(); ii++) {
					if (ii != 0) {
						move = move + " ";
					}
					move = move + Integer.toString(numEncrypted.get(ii));
				}
				curPlayer.setSavedEncrypted(move);
				curPlayer.setSavedPhrase(c.getPhrase());
				curPlayer.setSavedType('N');
				curPlayer.setSavedUserPhrase(userPhrase);
			}
		} else if (guess.toLowerCase().equals("reveal")) {
			System.out.println("> " + c.getPhrase());
			System.out.println("> Game Finished, you lost!");
			finished = true;
			curPlayer.addGameplayed();
		} else if (guess.toLowerCase().equals("exit")) {
			System.out.println("Exit selected, Writing player details to file");
			p.saveToFile("players.txt");
			System.exit(0);
		} else if (guess.toLowerCase().equals("undo")) {
			System.out.println("Enter leter you wish to undo.");
			System.out.print(">");
			char remove = scan1.nextLine().charAt(0);
			if (userPhrase.indexOf(remove) != -1) {
				userPhrase = userPhrase.replace(remove, ' ');
				System.out.println("The character '" + remove + "' has been removed.");
			} else {
				System.out.println("The character you entered has not been mapped.");
			}
		} else {
			curPlayer.addGuess();
			brokenString = guess.split(" ");
			letterguess = brokenString[0].charAt(0);
			position = Integer.valueOf(brokenString[1]);
			if (numEncrypted.indexOf(position) == -1) {
				System.out.println("That position does not exist in the encrypted string!");
			} else if (userString.contains(Character.toString(letterguess))) {
				System.out.println("you have already entered the letter " + letterguess);
			} else {
				intString = numEncrypted;
				if (userString.charAt(intString.indexOf(position)) != ' ') {
					System.out.println("> Are you sure you want to overwrite this answer?(y/n)");
					char remove = scan1.nextLine().charAt(0);
					if (remove == 'y') {
						userString = userString.replace(userString.charAt(intString.indexOf(position)), letterguess);
						System.out.println("> Your character was overwritten.");
						checked = true;
					} else if (remove == 'n') {
						System.out.println("> Your character was not overwritten.");
					}
				} else {
					for (int i = 0; i < intString.size(); i++) {
						if (intString.get(i) == position) {
							userString = userString.substring(0, i) + letterguess + userString.substring(i + 1);
						}
					}
				}
				userPhrase = userString;
			}

		}
	}

	public void displayStats() {
		System.out.println("> Stats for player " + curPlayer.getName());
		System.out.println("> Games Played: " + curPlayer.getGamesplayed());
		System.out.println("> Games Won: " + curPlayer.getGameswon());
		System.out.println("> Hints Used: " + curPlayer.getHintsUsed());
		System.out.println("> Total Guesses: " + curPlayer.getTotalGuesses());
		System.out.println("> Accuracy: " + curPlayer.getAccuracy());
	}

	public void displayCrypto() {
		System.out.println("Guess a Letter, then a space, then enter the letter you want it to replace!");
		System.out.println(
				"Enter 'hint' to get a hint, 'save' to save your progress, 'reveal' to reveal the answer, or 'undo' to undo your last guess!");
		System.out.println("> Letter Frequencies: ");
		System.out.println("> " + encrypted);
		System.out.println("> " + userPhrase);
		System.out.print("> ");
	}

	public void displayNumCrypto() {
		System.out.println("Guess a Letter, then a space, then enter the letter you want it to replace!");
		System.out.println(
				"Enter 'hint' to get a hint, 'save' to save your progress, 'reveal' to reveal the answer, or 'undo' to undo your last guess!");
		System.out.print("> ");
		for (int i = 0; i < numEncrypted.size(); i++) {
			if (numEncrypted.get(i) == 27) {
				System.out.print("   ");
			} else {
				System.out.print("." + numEncrypted.get(i));
			}
		}
		System.out.println();
		System.out.print("> ");
		for (int i = 0; i < userPhrase.length(); i++) {
			if (userPhrase.charAt(i) == ' ') {
				System.out.print("   ");
			} else {
				System.out.print("." + userPhrase.charAt(i));
			}
		}
		System.out.println();
		System.out.print("> ");
	}
}
