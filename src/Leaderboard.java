import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class Leaderboard {

    ArrayList<Player> players;

    public Leaderboard(ArrayList<Player> p){
        players=p;
    }

    public void displayLeaderboard(ArrayList<Player> p){
        updateLeaderboard(p);
        Collections.sort(players, Player.PlaGameWon);
        System.out.println("Name    Games Won");
        if(players.size()>=10){
            for (int i=0;i<10;i++){
                System.out.println(p.get(i).getName() + "   " + p.get(i).getGameswon());
            }
        }else{
            for (int i=0;i<players.size();i++){
                System.out.println(p.get(i).getName() + "   " + p.get(i).getGameswon());
            }
        }

    }

    public void updateLeaderboard(ArrayList<Player> p){
        players=p;

    }


    public static void main(String[] args){
        ArrayList<Player> p = new ArrayList<>();
        Player p1 = new Player("kevin");
        Player p2 = new Player("george", 10, 9, 0, 30, 25, "null", "null", "null", 'm');
        Player p3 = new Player("winner", 10, 30, 0, 30, 25, "null", "null", "null", 'm');
        Player p4 = new Player("test", 10, 29, 0, 30, 25, "null", "null", "null", 'm');
        Player p5 = new Player("abovekevin", 10, 1, 0, 30, 25, "null", "null", "null", 'm');
        Player p6 = new Player("middle", 10, 15, 0, 30, 25, "null", "null", "null", 'm');
        Player p7 = new Player("belowtest", 10, 28, 0, 30, 25, "null", "null", "null", 'm');
        Player p8 = new Player("notinlb", 10, 0, 0, 30, 25, "null", "null", "null", 'm');
        Player p9 = new Player("kevinbutin", 10, 13, 0, 30, 25, "null", "null", "null", 'm');
        Player p10 = new Player("in", 10, 20, 0, 30, 25, "null", "null", "null", 'm');
        Player p11 = new Player("notin", 10, 0, 0, 30, 25, "null", "null", "null", 'm');
        Player p12 = new Player("inlb", 10, 10, 0, 30, 25, "null", "null", "null", 'm');
        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);
        p.add(p5);
        p.add(p6);
        p.add(p7);
        p.add(p8);
        p.add(p9);
        p.add(p10);
        p.add(p11);
        p.add(p12);
        Leaderboard lb = new Leaderboard(p);
        lb.displayLeaderboard(p);

    }

}
